Welcome to AllatRa Ways of Working's documentation!
===================================================

Here are some bits & pieces of documentation to help you get up to speed
with our ways of working, the tools we use, processes follow and the
things we would be expecting from you when doing projects.

If anything is unclear or plain wrong please do notify us or better yet,
submit a pull request to the repository!

Enjoy!

.. toctree::
   :maxdepth: 2

   contribution/ten-commandments.md
   platforms.md
   tools.md
   process.md
   projects.md
   gitflow/readme.md
   angularjs/README.md
   contribution/contribution-guidelines.md
