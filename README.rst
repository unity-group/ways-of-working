Organization-shared style guides and practices
==============================================

Hey, hopefully you're here because you're about to embark on a
project with us.

These docs are automatically built & available at http://ways-of-working-in-allatra.rtfd.org/.

Build these docs locally with::

    $ virtualenv ve
    $ source ve/bin/activate
    (ve)$ pip install -r requirements.pip
    (ve)$ cd docs
    (ve)$ make html

Open `index.html` in `docs/_build/html`.


Copyright (c) Year(s), AllatRa <it@allatra.org>
================================================

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING